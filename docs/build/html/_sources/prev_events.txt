#################
Previous Seminars
#################

An Introduction to BIDS: Data Organization for Transparency and Reproducibility in Neuroimaging Research
========================================================================================================

Speaker: Amanda Bischoff-Grethe


Date: November 14th, 2018

:download:`Intro to BIDS_Bischoff-Grethe  </pdf/BIDS_Presentation_14NOV2018.pdf>`