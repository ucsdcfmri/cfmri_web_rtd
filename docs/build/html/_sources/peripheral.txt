##########################
Peripheral Equipment Usage
##########################

Projectors
==========

* In each control room there are two projectors - the Lower projector and the Upper projector.

* The Lower projector is the main one used by many researchers. Tthis uses the large projection screen that stands at the end of the table. This setup is helpful if your subject is claustrophobic.

* The Upper projector is used when you want to use the projection screen that goes inside the bore.

* Projector Power:
    * Both projectors can be turned on with the red button on the remote. Make sure you point the remote very close to the projector you want to use or you could accidently turn both of them on.

* To turn off the projector simply press the power button on the remote twice.

* Projector Source Selection:
    * If you are getting a no signal error message:
      - Make sure the VGA cable is attached to your computer.
      - Press the source bottom on the remote until VGA port A is selected then press the Enter button.

* Projector Resolution:
    * The native max resolution of the lower projector is 1280x800 and the upper projector is 1024x768.
    * Both can be set on your computer's "System Preferences" or "Control Panel" depending on which OS you are using. Please refer to your user manual.

  .. image:: images/projector/projector1.png
    :width: 1000

  .. image:: images/projector/projector2.png
    :width: 1000

Button Boxes
============

* The button box system at CFMRI contains the following components: 
    * Electronic Interface
    * Fiber Optic cable

* Response Pads (2 button and 4 buttons)
    * 2 and 4 button handhelds


* SET UP
    * To test the optical part of the system without a computer: 
      -The LEDs on the side of the interface box will light when the correesponding buttons is pressed. This means that the optical part of the system is working properly. 
      
      - Set the program switch for your specific application and handheld.
      - The program switch is on the front plate of the interface box (see picture below). The switch has a small arrow which points to a number as shown below. Use a small flat headed screw driver to turn the switch to other positions. 