#############################
Imaging Software Installation
#############################

Analysis of Functional Neuroimages (AFNI)
=========================================

`Mac OSX <https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/steps_mac.html>`_

`Linux, Ubuntu 18.04 <https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/steps_linux_ubuntu18.html>`_

`Linux, Fedora and Red Hat <https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/steps_linux_Fed_RH.html>`_

`Windows 10 <https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/steps_windows10.html>`_

If you already have AFNI installed, you can update using the command: 

.. code-block:: csh
   
   @update.afni.binaries -d

AFNI may say the version it is trying to update is no longer supported and recommend that you update to the latest version  

In that case, you can use the -package option to update to the version that AFNI recommends 

.. code-block:: csh
   
   @update.afni.binaries -package macos_10.12_local

FSL: FMRIB Software Library
===========================

`Mac OSX <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation/MacOsX>`_

`Linux <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation/Linux>`_

`Windows <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation/Windows>`_

FreeSurfer Software Suite
=========================

`Mac or Linux <https://surfer.nmr.mgh.harvard.edu/fswiki/DownloadAndInstall>`_

MATLAB
======

Create a mathorks account at https://www.mathworks.com/ using your UCSD email address
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.. image:: images/matlab_install/mat1.png
  :width: 500

Link to the UCSD license (if you are not connected to it by default)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.. image:: images/matlab_install/mat2.png
  :width: 500

The UCSD license should be listed as 1144083
""""""""""""""""""""""""""""""""""""""""""""
.. image:: images/matlab_install/mat3.png
  :width: 500

Click on the "products" tab at the top of your account page
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.. image:: images/matlab_install/mat4.png
  :width: 500

Download the latest release after providing your OS info
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.. image:: images/matlab_install/mat5.png
  :width: 500

.. image:: images/matlab_install/mat6.png
  :width: 500

Unzip the downloaded file
"""""""""""""""""""""""""
.. image:: images/matlab_install/mat7.png
  :width: 200

Open the disk image
"""""""""""""""""""
.. image:: images/matlab_install/mat8.png
  :width: 200

Run the installer
"""""""""""""""""
.. image:: images/matlab_install/mat9.png
  :width: 500

Log in using your Mathworks account credentials
"""""""""""""""""""""""""""""""""""""""""""""""
.. image:: images/matlab_install/mat10.png
  :width: 500

Accept the license agreement
""""""""""""""""""""""""""""
.. image:: images/matlab_install/mat11.png
  :width: 500

Select the campus license
"""""""""""""""""""""""""
.. image:: images/matlab_install/mat12.png
  :width: 500

Choose the install location (Applications or default is OK)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.. image:: images/matlab_install/mat13.png
  :width: 500

Select the products to install
""""""""""""""""""""""""""""""
* See the image for our recommendation
* Installing ALL products will result in a VERY long installation process

.. image:: images/matlab_install/mat14.png
  :width: 500

Install Matlab
""""""""""""""
.. image:: images/matlab_install/mat15.png
  :width: 500

.. image:: images/matlab_install/mat16.png
  :width: 500






